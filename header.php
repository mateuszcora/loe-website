<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage mateuszcora-loe
 * @since 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/style.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<style>
		body{
			background-image:url(<?php echo get_bloginfo('template_directory');?>/Images/bg.png);
		}
		.post{
			border: 10px solid transparent;
			border-image: url(<?php echo get_bloginfo('template_directory');?>/Images/Border3.png) 10 round;

			background-image:url(<?php echo get_bloginfo('template_directory');?>/Images/postbg3.png);
		}
		.featured-post{
			border: 10px solid transparent;
			border-image: url(<?php echo get_bloginfo('template_directory');?>/Images/Border-Featured.png) 10 round;
		}
	</style>
	
</head>

<body <?php echo body_class();?>>

			<nav class="navbar navbar-fixed-top <?php if(is_admin_bar_showing()){echo("admin-bar-space");}?>">
				<div class = "containter-fluid navigation-menu">
					<div class="navbar-header">
						<a id="logo" class="brand" href="<?php echo get_home_url();?>">
							<img src="<?php echo get_bloginfo('template_directory'); ?>/Images/shortlogo.png" class="img-brand">
						</a>
						<button type="button" class="navbar-toggle collapsed navbar-toggle-button navbar-right" data-toggle="collapse" data-target="#ID_navbar" aria-expanded="false">
							<span class="sr-only">Toggle navbar</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

	<!-- main menu -->
					<div class = "collapse navbar-collapse" id = "ID_navbar">
						<ul class="nav navbar-nav">
						<?php
						$menu = wp_get_nav_menu_object( "NavMenu" );
						if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
							$menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );
							if(isset($menu_items))
								foreach($menu_items as $item){
								?><li class="nav-item <?php echo check_active_menu($item);?>">
									<a href="<?php echo $item->url;?>"> <b><?php echo $item->title;?></b> </a>
								</li><?php
									}
						?></ul>
						<ul class="nav navbar-nav navbar-right play-now-wrapper">
<?php
	/* side menu */
						unset($menu_items);
						$menu = wp_get_nav_menu_object( "PlayNow" );
						if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
							$menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );
							if(isset($menu_items))
							foreach($menu_items as $item){?>
								<li class="nav-item navbar-right play-now">
									<a href="<?php echo $item->url;?>"> <?php echo $item->title;?> </a>
								</li><?php
									}?>

						</ul>
					</div>
				</div>
			</nav>
			<div class="logo-large">
				<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_bloginfo('template_directory');?>/Images/Header2.png" class="logo-img"></a>
			</div>
			
			
			<div class="fluid-container main">
