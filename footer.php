﻿<div class="row">
	<div id="footer" class="col-md-6 col-md-offset-3 row post footer">
		<div class="col-xs-4 footer-info">
			<small>
				Website powered by WordPress<br>
				Theme by Mateusz Cora<br>
			</small>
		</div>
		<div class="col-xs-8 footer-social">
			Follow us:</br>
			<a href="https://www.facebook.com/LoEWGame/" class="fa fa-facebook"></a>
			<a href="https://www.youtube.com/channel/UCgci3qJEOtlYSpPjoZ07P8w" class="fa fa-twitter"></a>
			<a href="https://twitter.com/LoEWGame" class="fa fa-youtube-play"></a>
		</div>
	</div>
<div>
<?php wp_footer(); ?>
</body>