<?php
/*
Template name: LoE-Lore
 */
 get_header();
 while ( have_posts() ) : the_post();
?>
	<div class="row">
		<div class="col-md-2 post page-sidebar affix fluid-containter" style = "min-height:500px">
		<ul class="lore-nav-menu" id = "ID_navbar">
			<?php
			$menu = wp_get_nav_menu_object( "LoreMenu" );
			if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
				$menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false, "menu_class" => "lore-nav-menu" ) );
				if(isset($menu_items))
					foreach($menu_items as $item){
					?><li class="lore-nav-menu-item <?php echo check_active_menu($item);?>">
						<a href="<?php echo $item->url;?>"> <b><?php echo $item->title;?></b> </a>
					</li>
					<?php
						}
			?>
		</ul>
			<!--/*wp_nav_menu( array(
				'menu'           => 'LoreMenu',
				'theme_location' => '__no_such_location',
				'fallback_cb'    => false // Do not fall back to wp_page_menu()
			) );*/ 
?>-->
		</div>

		<div class="col-md-9 col-md-offset-3 page-content">
			<div class="post">
				<div class="post-title">
					<h2><?php the_title();?></h2>
				</div>
				<div class="post-content">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</div>

<?php
endwhile;

get_footer();
?>