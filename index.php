<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage mateuszcora-loe
 * @since 0.1
 */

		get_header(); 
			if ( have_posts() ) :
				?>

				<?php
				$post_counter=0;
				$max_posts=get_option( 'posts_per_page' );
				$first_post_id = -1; //hope no posts have this ID - needed when the variable is not being set by the featured post
				/* finding the newest featured post */
				if(is_front_page() && !is_paged()){
					$feat_query = new WP_Query( 'category_name=Featured&posts_per_page=1' ); 
					while($feat_query->have_posts()) : $feat_query->the_post();

						get_template_part('post/post', 'first');
						$first_post_id = $post->ID;
					endwhile;
				}
				?>
				<div class="row"><?php
				/* Start the Loop */
				//$regular_query = new WP_Query('posts_per_page=' .  (get_option( 'posts_per_page' ) + 1));
				while ( have_posts() ) : the_post();
					if(!is_front_page()){
						get_template_part('post/post', 'page');
						}
					else if($post->ID != $first_post_id){
						get_template_part('post/post','post');
						$post_counter++;
						}
					if($post_counter%3 == 0){		
						?>

				</div>
				<div class="row"><?php
					}
					/*escape the loop if we have too many posts - the query is modified in functions.php to take one more post in case we use one of the posts from this query 
					 * for the main post
					 */
					if($post_counter >= $max_posts){
						break;
					}
					
					
				endwhile;?>
				
</div><!-- main div -->				
				</div class="row">
<?php

				/*pagination*/
				if(!is_single() && !is_page()){
					?>
					<div class="row">
						<div id="pagination" class="col-md-6 col-md-offset-3 row equal" aria-labelledby="Pagination">
							<div class="col-xs-2">
								<a href="<?php echo previous_posts(0, false);?>"><img src="<?php echo get_bloginfo('template_directory');?>\Images\Left.png"></a>
							</div>
							<div class="col-xs-8 page-numbers-div">
								<?php
								$big = 9999999999;
								$args = array(
										'format'             => '?paged=%#%',
										'total'              => $wp_query->max_num_pages,
										'current'			 => max( 1, get_query_var('paged') ),
										'prev_next'			 => false
										);
								echo paginate_links( $args );
					
								?>
							</div>
							<div class="col-xs-2" style="text-align:right">
								<a href="<?php echo next_posts(0, false);?>"><img src="<?php echo get_bloginfo('template_directory');?>\Images\Right.png"</img></a>
							</div>
						</div><!-- pagination -->
					</div>
					<?php }
				endif;
get_footer(); 
?>
