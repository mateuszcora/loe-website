﻿<div class="col-md-4">  <!-- indenting this line properly creates a non-breaking whitespace character, please don't touch this -->
						<div class="post <?php if(in_category("Featured")){ echo ("featured-post");}?>">
							<div class="post-title">
								<a href="<?php echo get_permalink()?>"><h3><?php the_title();?></h3></a>
							</div>
							<div class="post-featured-image">
								<?php
								echo the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive', 'title' => 'Feature image']);
								?>
							</div>
							<div class="post-content">
								<?php the_excerpt();?>
							</div>
							<div class="read-more">
								<a href = "<?php echo get_permalink()?>">Read more</a>
							</div>
						</div>
					</div>