﻿<div class="col-md-12">
	<div class="post">
		<div class="post-title">
			<h2><?php the_title();?></h2>
		</div>
		<div class="post-content">
			<?php the_content();?>
		</div>
	</div>
</div>