﻿
						<div class="row post featured-post first-post-buster">
							<div class="col-md-4  post-featured-image">
								<?php
								echo the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive', 'title' => 'Feature image']);?>
							
							</div>
							<div class="col-md-8">
								<div class="post-title">
									<a href="<?php echo get_permalink()?>"><h3><?php the_title();?></h3></a>
								</div>

								<div class="post-content">
									<?php the_excerpt();?>
								</div>
								<div class="read-more">
									<a href = "<?php echo get_permalink()?>">Read more</a>
								</div>
							</div>
						</div>
				