﻿<?php
    get_header();

    while ( have_posts() ) : the_post();
        ?>
            <div class="col-md-12">
			    <div class="post">
				    <div class="post-title">
					    <h2><?php the_title();?></h2>
				    </div>
				    <div class="post-content">
					    <?php the_content();?>
				    </div>
					<div class="post-author">
						<?php the_author();?>
					</div>
			    </div>
		    </div>
        <?php
    endwhile;
    get_footer();
?>