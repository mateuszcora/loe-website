<?php
add_theme_support( 'post-thumbnails' );
add_post_type_support( 'post', 'excerpt' );

function check_active_menu( $menu_item ) {
    $actual_link = ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ( $actual_link == $menu_item->url ) {
        return 'active';
    }

    return '';
}

function my_modify_main_query( $query ) {
	if ( $query->is_home() && $query->is_main_query() ) { // Run only on the homepage
		$query->query_vars['posts_per_page'] = get_option( 'posts_per_page' ) + 1; // one more post please
		}
}
// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'my_modify_main_query' );
?>